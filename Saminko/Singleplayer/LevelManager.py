import pygame
import os
import time

from Enemy import Enemy
from Platform import Platform
from Player import Player

pygame.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
font = pygame.font.Font('freesansbold.ttf', 216)

initial_health = 10


class LevelManager:
    def __init__(self):
        self.levels = []
        self.curr_level = 0

        for level in os.listdir("levels/"):
            if level != "how_to_write.txt":
                self.levels.append(level)

        self.score = 0

        self.player = None
        self.arrow_start_point = [0, 0]

        self.curr_level_enemies = []
        self.curr_level_platforms = []
        self.curr_level_arrows = []

        self.bonus_active = False
        self.streak = 0

    def next(self, screen):
        if self.levels:     # checking if it is not empty
            self.curr_level += 1

            self.next_level_screen(screen)    # screen with a text: level x
            time.sleep(1.5)

            self.load()
            self.levels.remove(self.levels[0])
        else:
            exit(1)

    def checking_level(self, screen):
        if not self.curr_level_enemies:     # next level after killing last enemy
            if self.curr_level > 0:     # to make a brief break after killing last enemy
                time.sleep(2)
            self.next(screen)

    def next_level_screen(self, screen):
        screen.fill(BLACK)
        new_level_text = font.render("level " + str(self.curr_level), True, WHITE)
        screen.blit(new_level_text, (140, 180))
        pygame.display.flip()

    def load(self):
        # reset
        self.curr_level_enemies.clear()
        self.curr_level_platforms.clear()
        self.curr_level_arrows.clear()
        self.bonus_active = False
        self.streak = 0

        # loading new level
        level = open("levels/" + self.levels[0], "r")

        player_position = level.readline().split()  # adding player
        self.player = Player(int(player_position[0]), int(player_position[1]), initial_health)
        self.arrow_start_point = [int(player_position[0]) + 70, int(player_position[1]) + 25]

        number_of_enemies = level.readline()
        for enemy in range(0, int(number_of_enemies)):  # adding enemies
            enemy_variables = level.readline().split()

            new_enemy = Enemy(int(enemy_variables[0]), int(enemy_variables[1]), int(enemy_variables[2]))
            self.curr_level_enemies.append(new_enemy)

        number_of_platforms = level.readline()
        for platform in range(0, int(number_of_platforms)):  # adding platforms
            platform_position = level.readline().split()

            new_platform = Platform(int(platform_position[0]), int(platform_position[1]))
            self.curr_level_platforms.append(new_platform)

        level.close()

    def add_arrow(self, arrow):
        self.curr_level_arrows.append(arrow)

    def add_score(self, score):
        self.score += score

    def increase_streak(self):
        self.streak += 1

    def set_streak(self, value):
        self.streak = value

    def set_bonus_active(self, active):
        self.bonus_active = active

    def get_arrow_start_point(self):
        return self.arrow_start_point

    def get_player(self):
        return self.player

    def get_my_player(self):
        return self.player

    def get_enemies(self):
        return self.curr_level_enemies

    def get_platforms(self):
        return self.curr_level_platforms

    def get_arrows(self):
        return self.curr_level_arrows

    def get_bonus_active(self):
        return self.bonus_active

    def get_streak(self):
        return self.streak

    def get_score(self):
        return self.score
