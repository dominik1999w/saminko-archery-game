import math

import pygame
from configuration.config import *


def mouse_start(level_manager, mouse_pos):
    if (level_manager.get_my_player().arrow_start_point[0] - mouse_pos[0]) ** 2 + (
            level_manager.get_my_player().arrow_start_point[1] - mouse_pos[1]) ** 2 <= 1000:
        return True


def draw_mouse(level_manager, screen, mouse_pos):
    pygame.draw.line(screen, darkpink, (mouse_pos[0], mouse_pos[1]),
                     (level_manager.get_my_player().arrow_start_point[0], level_manager.get_my_player().arrow_start_point[1]), 3)


def calculate_deg(a, b):
    if b[0] == a[0] and b[1] <= a[1]:
        return math.pi * 0.5
    if b[0] == a[0] and b[1] > a[1]:
        return -math.pi * 0.5
    if b[0] - a[0] > 0:
        return math.atan((a[1] - b[1]) / (b[0] - a[0]))
    else:
        return math.pi + math.atan((a[1] - b[1]) / (b[0] - a[0]))


def get_pos(str):
    str = str.split(",")
    return float(str[0]), float(str[1]), int(str[2])


def set_pos(tmp):
    return str(tmp[0]) + "," + str(tmp[1]) + "," + str(tmp[2])
