import pygame

from Arrow import Arrow

plat_size_x = 100
plat_size_y = 20


class Platform:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.size_x = plat_size_x
        self.size_y = plat_size_y
        self.top_left = (x - self.size_x / 2, y - self.size_y / 2)
        self.bot_right = (x + self.size_x / 2, y + self.size_y / 2)

        self.arrows_stuck = []

    def draw_multi(self, screen, player_nr):
        pygame.draw.rect(screen, (255, 255, 255),
                         [self.top_left[0], self.top_left[1], self.bot_right[0] - self.top_left[0],
                          self.bot_right[1] - self.top_left[1]])

        for arrow in self.arrows_stuck:
            if player_nr == 0:
                arrow.draw_multi(screen, True)
            else:
                arrow.draw_multi(screen, False)

    def draw(self, screen):
        pygame.draw.rect(screen, (255, 255, 255),
                         [self.top_left[0], self.top_left[1], self.bot_right[0] - self.top_left[0],
                          self.bot_right[1] - self.top_left[1]])

        for arrow in self.arrows_stuck:
            arrow.draw(screen)

    def hit(self, arrow_x, arrow_y, arrow_deg):
        new_arrow = Arrow(arrow_x, arrow_y, arrow_deg, 0)
        self.arrows_stuck.append(new_arrow)

    def get_position(self):
        return [self.x, self.y]
