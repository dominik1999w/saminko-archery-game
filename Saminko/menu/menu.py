from configuration.config import *


def text_format(pygame, message, textFont, textSize, textColor):
    new_font = pygame.font.Font(textFont, textSize)
    new_text = new_font.render(message, True, textColor)

    return new_text


def menu(pygame, screen):
    intro = True
    selected = "Singleplayer"

    while intro:
        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            exit(1)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                selected = "Singleplayer"
            elif event.key == pygame.K_DOWN:
                selected = "Multiplayer"
            if event.key == pygame.K_RETURN:
                return selected
        screen.fill(black)
        title = text_format(pygame, "Saminko", font_style, title_size, white)
        screen.blit(title, ((screen_width - title.get_width()) / 2, TITLE_TEXT))
        if selected == "Singleplayer":
            single_player = text_format(pygame, "Singleplayer", font_style, text_size, blue)
        else:
            single_player = text_format(pygame, "Singleplayer", font_style, text_size, yellow)
        if selected == "Multiplayer":
            multi_player = text_format(pygame, "Multiplayer", font_style, text_size, blue)
        else:
            multi_player = text_format(pygame, "Multiplayer", font_style, text_size, yellow)
        screen.blit(single_player, ((screen_width - single_player.get_width()) / 2, SINGLEPLAYER_TEXT))
        screen.blit(multi_player, ((screen_width - multi_player.get_width()) / 2, MULTIPLAYER_TEXT))
        pygame.display.flip()
