# COLORS
white = (255, 255, 255)
black = (0, 0, 0)
gray = (50, 50, 50)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
yellow = (255, 255, 0)
darkpink = (121, 0, 0)
############################################


# SCREEN OPTIONS
screen_width = 1000
screen_height = 600
SINGLEPLAYER_TEXT = 250
MULTIPLAYER_TEXT = 350
TITLE_TEXT = 150
text_size = 30
title_size = 50
font_style = 'freesansbold.ttf'
font_size = 32
############################################

# LEVELS MANAGEMENT
