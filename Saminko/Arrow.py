import pygame
import math

# ARROW VARIABLES

arrow_len = 50
arrow_width = 30
arrow_vel = 12

# PHYSICS VARIABLES
gravity = 0.18


# FUNCTIONS
def rect_intersection(x, y, top_left, bot_right):
    if top_left[0] < x < bot_right[0] and top_left[1] < y < bot_right[1]:
        return True
    return False


def circle_intersection(x, y, center, radius):
    if math.sqrt((x - center[0]) ** 2 + (y - center[1]) ** 2) < radius:
        return True
    return False


def bound_intersection(x, y, screen_boundaries):  # delete object, Arrow, when not needed
    if x > screen_boundaries[0] + arrow_len or y > screen_boundaries[1] + arrow_len:
        return True
    return False


class Arrow:
    def __init__(self, x, y, deg, start_time, bonus_arrow=False):  # (x,y) == arrowhead
        self.pos_x = x  # (pos_x, pos_y) == start position
        self.pos_y = y

        self.x = x      # (x, y) == current position
        self.y = y

        self.deg = deg  # start angle
        self.delta_deg = deg    # current angle

        self.velocity = arrow_vel   # current arrow velocity

        self.start_time = start_time    # moment of shot

        self.bonus_hit = False  # checking if this arrow hit a bonus circle

        if bonus_arrow:     # checking if the arrow is one of three shot as a bonus shot
            self.bonus_arrow = True
        else:
            self.bonus_arrow = False

    def draw_multi(self, screen, left=True):
        x = self.x
        y = self.y
        delta_deg = self.delta_deg

        if self.delta_deg == math.pi / 2:
            end_x = x + arrow_len * math.cos(delta_deg)
            end_y = y + arrow_len
        elif self.delta_deg == -math.pi / 2:
            end_x = x - arrow_len * math.cos(delta_deg)
            end_y = y - arrow_len
        elif (-math.pi / 2 < self.deg < math.pi / 2 and left) or ((self.deg < - math.pi/2 or self.deg > math.pi/2) and not left):
            end_x = x - arrow_len * math.cos(delta_deg)
            end_y = y + arrow_len * math.sin(delta_deg)
        else:
            end_x = x + arrow_len * math.cos(delta_deg)
            end_y = y - arrow_len * math.sin(delta_deg)

        cross_x = x - (x - end_x) / 5
        cross_y = y - (y - end_y) / 5

        arrow_up_x = cross_x - arrow_len / 5 * math.sin(delta_deg)
        arrow_up_y = cross_y - arrow_len / 5 * math.cos(delta_deg)
        arrow_down_x = cross_x + arrow_len / 5 * math.sin(delta_deg)
        arrow_down_y = cross_y + arrow_len / 5 * math.cos(delta_deg)

        pygame.draw.line(screen, (255, 255, 255), (x, y), (end_x, end_y), 2)
        pygame.draw.polygon(screen, (255, 255, 255), [(x, y), (arrow_up_x, arrow_up_y), (arrow_down_x, arrow_down_y)])

    def draw(self, screen):
        x = self.x
        y = self.y
        delta_deg = self.delta_deg

        if self.delta_deg == math.pi/2:
            end_x = x + arrow_len * math.cos(delta_deg)
            end_y = y + arrow_len
        elif self.delta_deg == -math.pi/2:
            end_x = x - arrow_len * math.cos(delta_deg)
            end_y = y - arrow_len
        elif -math.pi/2 < self.deg < math.pi/2:
            end_x = x - arrow_len * math.cos(delta_deg)
            end_y = y + arrow_len * math.sin(delta_deg)
        else:
            end_x = x + arrow_len * math.cos(delta_deg)
            end_y = y - arrow_len * math.sin(delta_deg)

        cross_x = x - (x - end_x) / 5
        cross_y = y - (y - end_y) / 5

        arrow_up_x = cross_x - arrow_len / 5 * math.sin(delta_deg)
        arrow_up_y = cross_y - arrow_len / 5 * math.cos(delta_deg)
        arrow_down_x = cross_x + arrow_len / 5 * math.sin(delta_deg)
        arrow_down_y = cross_y + arrow_len / 5 * math.cos(delta_deg)

        pygame.draw.line(screen, (255, 255, 255), (x, y), (end_x, end_y), 2)
        pygame.draw.polygon(screen, (255, 255, 255), [(x, y), (arrow_up_x, arrow_up_y), (arrow_down_x, arrow_down_y)])

    def move(self, time):
        delta_time = time - self.start_time

        self.x = self.pos_x + self.velocity * delta_time * math.cos(self.deg)
        self.y = self.pos_y - self.velocity * delta_time * math.sin(self.deg) + gravity / 2 * delta_time ** 2

        if self.deg == math.pi/2:
            if self.velocity * math.sin(self.deg) - gravity * delta_time >= 0:
                self.delta_deg = math.pi/2
            else:
                self.delta_deg = -math.pi/2
        else:
            self.delta_deg = math.atan(
                (self.velocity * math.sin(self.deg) - gravity * delta_time) / (self.velocity * math.cos(self.deg)))

    def check_hit_multi(self, level_manager, screen_boundaries):
        # return without bonus: -1: missed, 1: hit enemy, 2: hit bonus, 3: hit player
        # return with bonus: -11: missed, 11: hit enemy, 2: hit bonus, 13: hit player

        # HITTING PLAYER
        player_left = level_manager.get_player_left()
        player_right = level_manager.get_player_right()

        # checking head shot
        if circle_intersection(self.x, self.y, player_left.head_center, player_left.head_radius):

            level_manager.get_arrows().remove(self)
            player_left.hit(self.x, self.y, self.delta_deg, True, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3
        elif circle_intersection(self.x, self.y, player_right.head_center, player_right.head_radius):

            level_manager.get_arrows().remove(self)
            player_right.hit(self.x, self.y, self.delta_deg, True, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3
        # checking hit
        elif rect_intersection(self.x, self.y, player_left.top_left, player_left.bot_right):
            level_manager.get_arrows().remove(self)
            player_left.hit(self.x, self.y, self.delta_deg, False, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3
        elif rect_intersection(self.x, self.y, player_right.top_left, player_right.bot_right):
            level_manager.get_arrows().remove(self)
            player_right.hit(self.x, self.y, self.delta_deg, False, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3

        # HITTING PLATFORMS
        for platform in level_manager.get_platforms():
            if rect_intersection(self.x, self.y, platform.top_left, platform.bot_right):
                level_manager.get_arrows().remove(self)
                platform.hit(self.x, self.y, self.delta_deg)

                if not self.bonus_hit:  # we don't want to make streak = 0 when we hit bonus
                    if self.bonus_arrow:
                        return -11
                    else:
                        return -1

        # HITTING BOUNDARIES
        if bound_intersection(self.x, self.y, screen_boundaries):
            level_manager.get_arrows().remove(self)

            if not self.bonus_hit:  # we don't want to make streak = 0 when we hit bonus
                if self.bonus_arrow:
                    return -11
                else:
                    return -1

        # DID NOT CONNECT
        return 0

    def check_hit(self, level_manager, screen_boundaries, bonus=None):
        # return without bonus: -1: missed, 1: hit enemy, 2: hit bonus, 3: hit player
        # return with bonus: -11: missed, 11: hit enemy, 2: hit bonus, 13: hit player

        # HITTING PLAYER
        player = level_manager.get_player()

        # checking head shot
        if circle_intersection(self.x, self.y, player.head_center, player.head_radius):

            level_manager.get_arrows().remove(self)
            player.hit(self.x, self.y, self.delta_deg, True, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3
        # checking hit
        elif rect_intersection(self.x, self.y, player.top_left, player.bot_right):
            level_manager.get_arrows().remove(self)
            player.hit(self.x, self.y, self.delta_deg, False, level_manager)

            if self.bonus_arrow:
                return 13
            else:
                return 3

        # HITTING ENEMIES
        for enemy in level_manager.get_enemies():
            # checking head shot
            if circle_intersection(self.x, self.y, enemy.head_center, enemy.head_radius):
                level_manager.get_arrows().remove(self)
                enemy.hit(self.x, self.y, self.delta_deg, True, level_manager)

                if self.bonus_arrow:
                    return 11
                else:
                    return 1
            # checking hit
            elif rect_intersection(self.x, self.y, enemy.top_left, enemy.bot_right):
                level_manager.get_arrows().remove(self)
                enemy.hit(self.x, self.y, self.delta_deg, False, level_manager)

                if self.bonus_arrow:
                    return 11
                else:
                    return 1

        # HITTING PLATFORMS
        for platform in level_manager.get_platforms():
            if rect_intersection(self.x, self.y, platform.top_left, platform.bot_right):
                level_manager.get_arrows().remove(self)
                platform.hit(self.x, self.y, self.delta_deg)

                if not self.bonus_hit:  # we don't want to make streak = 0 when we hit bonus
                    if self.bonus_arrow:
                        return -11
                    else:
                        return -1

        # HITTING BOUNDARIES
        if bound_intersection(self.x, self.y, screen_boundaries):
            level_manager.get_arrows().remove(self)

            if not self.bonus_hit:  # we don't want to make streak = 0 when we hit bonus
                if self.bonus_arrow:
                    return -11
                else:
                    return -1

        # HITTING BONUS
        if not bonus:
            return 0
        if bonus.exists and circle_intersection(self.x, self.y, (bonus.x, bonus.y), bonus.rad):
            self.bonus_hit = True
            return 2

        # DID NOT CONNECT
        return 0
