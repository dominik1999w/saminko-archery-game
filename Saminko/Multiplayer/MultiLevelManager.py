import pygame
from Platform import Platform
from Player import Player

pygame.init()

font = pygame.font.Font('freesansbold.ttf', 216)

initial_health = 10
player_start_point_left = [80, 275]
arrow_start_point_left = [150, 300]  # 15% from left

player_start_point_right = [841, 275]
arrow_start_point_right = [850, 300]  # 15% from right


class MultiLevelManager:
    def __init__(self, player_nr):
        self.player_nr = player_nr

        self.score = 0
        self.player_left = Player(player_start_point_left[0], player_start_point_left[1], initial_health, 0)
        self.player_right = Player(player_start_point_right[0], player_start_point_right[1], initial_health, 1)

        self.enemies = []
        self.platforms = []
        self.add_platforms()

        self.arrows = []

        self.bonus_active = False
        self.streak = 0

    def add_arrow(self, arrow):
        self.arrows.append(arrow)

    def add_score(self, score):
        self.score += score

    def increase_streak(self):
        self.streak += 1

    def add_platforms(self):
        self.platforms.append(Platform(100, 400))
        self.platforms.append(Platform(200, 400))
        self.platforms.append(Platform(800, 400))
        self.platforms.append(Platform(900, 400))

    def set_streak(self, value):
        self.streak = value

    def set_bonus_active(self, active):
        self.bonus_active = active

    def get_player(self):
        if self.player_nr == 0:     # opponent
            return self.player_right
        else:
            return self.player_left

    def get_my_player(self):
        if self.player_nr == 0:
            return self.player_left
        else:
            return self.player_right

    def get_player_left(self):
        return self.player_left

    def get_player_right(self):
        return self.player_right

    def get_enemies(self):
        return self.enemies

    def get_platforms(self):
        return self.platforms

    def get_arrows(self):
        return self.arrows

    def get_bonus_active(self):
        return self.bonus_active

    def get_streak(self):
        return self.streak

    def get_score(self):
        return self.score
