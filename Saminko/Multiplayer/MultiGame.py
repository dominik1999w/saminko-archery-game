from Arrow import Arrow, arrow_vel, arrow_len
from Multiplayer.MultiLevelManager import MultiLevelManager
from configuration.DrawFunc import *
from configuration.config import *
from server.network import Network

pygame.init()


def run(screen, clock):
    # ARROWS
    curr_arrow = None

    # MOUSE
    mouse_done = False
    mouse_pressed = False

    # TIMERS
    time = 0
    last_shot = 0
    shot_interval = 60

    network = Network()
    tmp = set_pos((0.0, -1.0, 0))
    decide_player = get_pos(network.send(tmp))
    if decide_player[2] == 0:
        playerNr = 0
    else:
        playerNr = 1
    print(playerNr)

    shoot = False
    # LEVEL MANAGER
    level_manager = MultiLevelManager(playerNr)

    # JUMPING
    can_jump = True

    while True:
        # TIMER
        time += 1
        clock.tick(90)

        if shoot:
            enemy_arrow = get_pos(network.send(set_pos((curr_arrow.deg, curr_arrow.velocity, -1))))
            shoot = False
        else:
            enemy_arrow = get_pos(network.send(set_pos((-1, -1, -1))))

        if enemy_arrow[1] != -1.0:
            pos = level_manager.get_player().arrow_start_point

            new_arrow = Arrow(pos[0], pos[1], enemy_arrow[0], time)
            new_arrow.velocity = enemy_arrow[1]
            level_manager.add_arrow(new_arrow)

        # EXIT AND MOUSE MANAGEMENT
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                exit(1)
            if (
                    time - last_shot > shot_interval or last_shot == 0) \
                    and e.type == pygame.MOUSEBUTTONDOWN and mouse_start(
                level_manager,
                pygame.mouse.get_pos()):
                mouse_pressed = True
            if mouse_pressed and e.type == pygame.MOUSEBUTTONUP:
                mouse_pressed = False

            if e.type == pygame.KEYDOWN:
                # movement (saving information about keys being pressed)
                if e.key == pygame.K_w and can_jump:
                    level_manager.get_my_player().jump()
                if e.key == pygame.K_a:
                    level_manager.get_my_player().set_key_clicked("left", True)
                if e.key == pygame.K_d:
                    level_manager.get_my_player().set_key_clicked("right", True)

            # movement (saving information about keys stop being pressed)
            if e.type == pygame.KEYUP:
                if e.key == pygame.K_a:
                    level_manager.get_my_player().set_key_clicked("left", False)
                if e.key == pygame.K_d:
                    level_manager.get_my_player().set_key_clicked("right", False)

        screen.fill(black)

        # PLATFORMS
        for platform in level_manager.get_platforms():
            platform.draw_multi(screen, playerNr)

        # MOVEMENT
        horizontal_velocity = 0
        if level_manager.get_my_player().key_clicked["left"] and level_manager.get_my_player().key_clicked["right"]:
            horizontal_velocity = 0
        elif level_manager.get_my_player().key_clicked["left"]:
            horizontal_velocity = -1
        elif level_manager.get_my_player().key_clicked["right"]:
            horizontal_velocity = 1

        level_manager.get_my_player().set_velocity([horizontal_velocity, 0])

        can_jump = level_manager.get_my_player().gravity(level_manager.get_platforms())
        level_manager.get_my_player().move()

        # DRAWING ARCHERS
        level_manager.get_player_left().draw_multi(screen, True)
        level_manager.get_player_right().draw_multi(screen, False)

        # ARROWS
        for arrow in level_manager.get_arrows():
            arrow.move(time)
            arrow.draw_multi(screen)

            tmp = arrow.check_hit_multi(level_manager, [screen_width, screen_height])
            if tmp == 3 or tmp == 13:  # player hit
                print("player hit")

        start_point = level_manager.get_my_player().arrow_start_point

        # MOUSE
        if mouse_pressed:
            mouse_done = True
            tmp_deg = calculate_deg(pygame.mouse.get_pos(), start_point)

            curr_arrow = Arrow(start_point[0], start_point[1], tmp_deg, time)

            draw_mouse(level_manager, screen, pygame.mouse.get_pos())

        if mouse_done and not mouse_pressed:
            mouse_done = False

            force = math.sqrt(
                (start_point[0] - pygame.mouse.get_pos()[0]) ** 2 + (start_point[1] - pygame.mouse.get_pos()[1]) ** 2)

            curr_arrow.velocity = arrow_vel

            if 1.3 < force / arrow_len <= 3:
                curr_arrow.velocity = arrow_vel * force / arrow_len * 0.5
            elif force / arrow_len > 3:
                curr_arrow.velocity = arrow_vel * 1.2

            else:
                curr_arrow.velocity *= force / arrow_len * 0.5

            shoot = True
            level_manager.add_arrow(curr_arrow)

            last_shot = time

        # DISPLAY OVERWRITING
        pygame.display.flip()
