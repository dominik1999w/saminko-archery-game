from Arrow import Arrow, arrow_vel, arrow_len
from Bonus import Bonus
from Singleplayer.LevelManager import LevelManager
from configuration.DrawFunc import *
from configuration.config import *


def run(screen, clock, font):
    # LEVEL MANAGER
    level_manager = LevelManager()
    # ARROWS
    curr_arrow = None
    curr_arrow2 = None
    curr_arrow3 = None
    streak = 0
    # MOUSE
    mouse_done = False
    mouse_pressed = False
    # TIMERS
    last_shot = 0
    shot_interval = 60
    time = 0
    # BONUS
    bonus = None
    bonus_lock = False
    super_arrow = False
    multiple_arrows = False
    bonus_arrows = 0

    while True:
        # TIMER
        time += 1
        clock.tick(90)

        # EXIT AND MOUSE MANAGEMENT
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                exit(1)
            if (
                    time - last_shot > shot_interval or last_shot == 0) and e.type == pygame.MOUSEBUTTONDOWN and mouse_start(
                level_manager,
                pygame.mouse.get_pos()):
                mouse_pressed = True
            if mouse_pressed and e.type == pygame.MOUSEBUTTONUP:
                mouse_pressed = False

        screen.fill(black)

        # LOADING LEVELS
        level_manager.checking_level(screen)

        # PLATFORMS
        for platform in level_manager.get_platforms():
            platform.draw(screen)

        # ARCHER
        level_manager.get_player().draw(screen)

        # ARROWS
        for arrow in level_manager.get_arrows():
            arrow.move(time)
            arrow.draw(screen)
            if level_manager.get_bonus_active():
                tmp = arrow.check_hit(level_manager, [screen_width, screen_height], bonus)
            else:
                tmp = arrow.check_hit(level_manager, [screen_width, screen_height])

            if tmp == 1:  # hit an enemy without bonus
                level_manager.add_score(1)
                level_manager.increase_streak()
            elif tmp == 11:  # hit an enemy with bonus
                level_manager.add_score(1)
                bonus_lock = False
                level_manager.increase_streak()
            elif tmp == -1:  # missed without bonus
                level_manager.set_streak(0)
            elif tmp == -11:  # missed with bonus
                bonus_lock = False
                bonus_arrows -= 1
                if bonus_arrows == 0:
                    level_manager.set_streak(0)
            elif tmp == 2:  # bonus activated
                level_manager.increase_streak()
                super_arrow = True  # to know that next shot is with bonus
                level_manager.set_bonus_active(False)  # to not display bonus
                bonus_lock = True  # to not have multiple bonus
            elif tmp == 3 or tmp == 13:  # player hit
                print("player hit")

        # STREAK
        streak = level_manager.get_streak()
        text = font.render(str(streak), True, white)
        screen.blit(text, (10, 10))

        if streak != 0 and streak % 3 == 0 and not level_manager.get_bonus_active() and not bonus_lock:
            bonus = Bonus(screen_width, screen_height)
            level_manager.set_bonus_active(True)
        if level_manager.get_bonus_active():
            bonus.draw(screen)

        # SCORE
        score_text = font.render("score: " + str(level_manager.get_score()), True, white)
        screen.blit(score_text, (800, 10))

        # ENEMIES
        for enemy in level_manager.get_enemies():
            enemy.draw(screen)

        start_point = level_manager.get_my_player().arrow_start_point

        # MOUSE
        if mouse_pressed:
            mouse_done = True
            tmp_deg = calculate_deg(pygame.mouse.get_pos(), start_point)

            if super_arrow:
                curr_arrow = Arrow(start_point[0], start_point[1], tmp_deg, time, True)
                curr_arrow2 = Arrow(start_point[0], start_point[1], tmp_deg + 0.1, time, True)
                curr_arrow3 = Arrow(start_point[0], start_point[1], tmp_deg - 0.1, time, True)
                multiple_arrows = True
            else:
                curr_arrow = Arrow(start_point[0], start_point[1], tmp_deg, time)

            draw_mouse(level_manager, screen, pygame.mouse.get_pos())

        if mouse_done and not mouse_pressed:
            mouse_done = False

            force = math.sqrt(
                (start_point[0] - pygame.mouse.get_pos()[0]) ** 2 + (
                        start_point[1] - pygame.mouse.get_pos()[1]) ** 2)

            curr_arrow.velocity = arrow_vel

            if 1.3 < force / arrow_len <= 3:
                curr_arrow.velocity = arrow_vel * force / arrow_len * 0.5
                if super_arrow:
                    curr_arrow2.velocity = arrow_vel * force / arrow_len * 0.5
                    curr_arrow3.velocity = arrow_vel * force / arrow_len * 0.5
            elif force / arrow_len > 3:
                curr_arrow.velocity = arrow_vel * 1.2
                if super_arrow:
                    curr_arrow2.velocity = arrow_vel * 1.2
                    curr_arrow3.velocity = arrow_vel * 1.2
            else:
                curr_arrow.velocity *= force / arrow_len * 0.5
                if super_arrow:
                    curr_arrow2.velocity *= force / arrow_len * 0.5
                    curr_arrow3.velocity *= force / arrow_len * 0.5

            level_manager.add_arrow(curr_arrow)
            if multiple_arrows:
                level_manager.add_arrow(curr_arrow2)
                level_manager.add_arrow(curr_arrow3)
                bonus_arrows = 3
                super_arrow = False
            multiple_arrows = False
            last_shot = time

        # DISPLAY OVERWRITING
        pygame.display.flip()
