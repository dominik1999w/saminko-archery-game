import pygame
import random

WHITE = (255, 255, 255)


class Bonus:
    def __init__(self, screen_width, screen_height):
        self.exists = True

        self.rad = 20
        self.x = random.randint(int(screen_width / 3), int(screen_width * 3 / 4))
        self.y = random.randint(self.rad, screen_height - self.rad)

    def draw(self, screen):
        pygame.draw.circle(screen, WHITE, (self.x, self.y), self.rad)
