from Multiplayer import MultiGame
from Singleplayer import SingleGame
import pygame
from configuration.config import *
from menu.menu import menu

pygame.init()

screen = pygame.display.set_mode([screen_width, screen_height])
clock = pygame.time.Clock()
pygame.display.set_caption("Saminko")
font = pygame.font.Font(font_style, font_size)
if menu(pygame, screen) == "Singleplayer":
    SingleGame.run(screen, clock, font)
else:
    MultiGame.run(screen, clock)
