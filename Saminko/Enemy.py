import pygame

from Arrow import Arrow

enemy_archer_image = pygame.image.load("pics/enemy_archer.png")

WHITE = (255, 255, 255)
RED = (255, 0, 0)

hit_damage = 1
head_shot_damage = 5


class Enemy:
    def __init__(self, pos_x, pos_y, initial_health):
        self.pos_x = pos_x
        self.pos_y = pos_y

        self.top_left = (pos_x + 35, pos_y + 20)
        self.bot_right = (pos_x + 65, pos_y + 78)

        self.head_center = (pos_x + 49, pos_y + 11)
        self.head_radius = 14

        self.initial_health = initial_health
        self.health = initial_health  # it will be changed after hit

        self.health_bar_start = (self.pos_x + 10, self.pos_y - 25)
        self.health_bar_size_x = 80
        self.health_bar_size_y = 8

        self.arrows_stuck = []

    def draw(self, screen):
        screen.blit(enemy_archer_image, (self.pos_x, self.pos_y))

        for arrow in self.arrows_stuck:
            arrow.draw(screen)

        self.draw_health(screen)

    def draw_health(self, screen):
        pygame.draw.rect(screen, RED, [self.health_bar_start[0], self.health_bar_start[1], self.health_bar_size_x,
                                       self.health_bar_size_y], 1)
        pygame.draw.rect(screen, RED, [self.health_bar_start[0], self.health_bar_start[1],
                                       self.health_bar_size_x * self.health / self.initial_health,
                                       self.health_bar_size_y])

    def check_death(self, level_manager):
        if self.health <= 0:
            level_manager.add_score(10)
            self.arrows_stuck.clear()
            level_manager.get_enemies().remove(self)

    def hit(self, arrow_x, arrow_y, arrow_deg, head_shot, level_manager):
        if head_shot:
            self.health -= head_shot_damage
        else:
            self.health -= hit_damage
        self.check_death(level_manager)

        new_arrow = Arrow(arrow_x, arrow_y, arrow_deg, 0)
        self.arrows_stuck.append(new_arrow)
