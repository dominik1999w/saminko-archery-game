import socket
from _thread import *


localIP = "127.0.0.1" # put yours
server = localIP
port = 5556  # any open port will do a job!

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((localIP, port))
except socket.error as error:
    str(error)

s.listen(2)  # listen for connection
print("server ready!")


def get_pos(str):
    str = str.split(",")
    return float(str[0]), float(str[1]), int(str[2])


def set_pos(tmp):
    return str(tmp[0]) + "," + str(tmp[1]) + "," + str(tmp[2])


deg = [0.0, 0.0]
vel = [-1.0, -1.0]


def threaded_client(c, player):
    c.send(str.encode(set_pos((-2, -2, player))))
    while True:
        try:
            data = get_pos(c.recv(2048).decode())
            deg[player] = data[0]
            vel[player] = data[1]
            if not data:
                print("disconnected")
                break
            else:
                reply = player
                if player == 0:
                    reply = (deg[1], vel[1], 0)
                elif player == 1:
                    reply = (deg[0], vel[0], 1)
            c.sendall(str.encode(set_pos(reply)))
        except:
            break
    print("Lost connection")
    c.close()


playerNumber = 0

while True:
    c, addr = s.accept()
    print("conneected to", addr)
    start_new_thread(threaded_client, (c, playerNumber))
    playerNumber += 1
