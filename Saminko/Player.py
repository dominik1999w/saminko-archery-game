import pygame

from Arrow import Arrow

archer_image = pygame.image.load("pics/archer.png")
enemy_archer = pygame.image.load("pics/enemy_archer.png")

RED = (255, 0, 0)

head_shot_damage = 5
hit_damage = 1
player_speed = 2
player_gravity = 0.3


class Player:
    def __init__(self, pos_x, pos_y, initial_health, playerNr=0):
        self.pos_x = pos_x
        self.pos_y = pos_y

        self.top_left = [pos_x + 35, pos_y + 20]
        self.bot_right = [pos_x + 65, pos_y + 78]

        self.head_center = [pos_x + 49, pos_y + 11]
        self.head_radius = 14

        self.initial_health = initial_health
        self.health = initial_health  # it will be changed after hit

        self.health_bar_start = [self.pos_x + 10, self.pos_y - 25]
        self.health_bar_size_x = 80
        self.health_bar_size_y = 8

        if playerNr == 0:
            self.arrow_start_point = [150, 300]
        else:
            self.arrow_start_point = [850, 300]

        self.arrows_stuck = []

        # checking if player is moving
        self.key_clicked = {"left": False, "right": False}

        self.velocity = [0, 0]
        self.speed = player_speed

    def set_key_clicked(self, direction, click):  # setting that the movement key is (not) being pressed
        self.key_clicked[direction] = click

    def jump(self):
        self.velocity[1] = - 3 * self.speed

    def gravity(self, platforms):
        self.velocity[1] += player_gravity

        colliding = False
        for platform in platforms:
            if self.pos_y + 79 > platform.get_position()[1] and self.velocity[1] > 0:
                colliding = True

        if colliding:
            self.velocity[1] = 0

        return colliding

    def set_velocity(self, direction):
        self.velocity[0] = direction[0] * self.speed

    def move(self):
        self.pos_y += self.velocity[1]
        self.pos_x += self.velocity[0]

        self.top_left[0] += self.velocity[0]
        self.top_left[1] += self.velocity[1]

        self.bot_right[0] += self.velocity[0]
        self.bot_right[1] += self.velocity[1]

        self.head_center[0] += self.velocity[0]
        self.head_center[1] += self.velocity[1]

        self.health_bar_start[0] += self.velocity[0]
        self.health_bar_start[1] += self.velocity[1]

        self.arrow_start_point[0] += self.velocity[0]
        self.arrow_start_point[1] += self.velocity[1]


    def draw(self, screen):
        screen.blit(archer_image, (self.pos_x, self.pos_y))
        pygame.draw.circle(screen, RED, self.arrow_start_point, 3)
        for arrow in self.arrows_stuck:
            arrow.draw(screen)

        self.draw_health(screen)

    def draw_multi(self, screen, left):
        pygame.draw.circle(screen, RED, [int(self.arrow_start_point[0]), int(self.arrow_start_point[1])], 3)
        if left:
            screen.blit(archer_image, (self.pos_x, self.pos_y))
        else:
            screen.blit(enemy_archer, (self.pos_x, self.pos_y))

        for arrow in self.arrows_stuck:
            arrow.draw_multi(screen, not left)

        self.draw_health(screen)

    def draw_health(self, screen):
        pygame.draw.rect(screen, RED, [self.health_bar_start[0], self.health_bar_start[1], self.health_bar_size_x,
                                       self.health_bar_size_y], 1)
        pygame.draw.rect(screen, RED, [self.health_bar_start[0], self.health_bar_start[1],
                                       self.health_bar_size_x * self.health / self.initial_health,
                                       self.health_bar_size_y])

    def check_death(self, level_manager):
        if self.health <= 0:
            level_manager.add_score(50)
            self.arrows_stuck.clear()
            print("you lose")

    def hit(self, arrow_x, arrow_y, arrow_deg, head_shot, level_manager):
        if head_shot:
            self.health -= head_shot_damage
        else:
            self.health -= hit_damage
        self.check_death(level_manager)

        new_arrow = Arrow(arrow_x, arrow_y, arrow_deg, 0)
        self.arrows_stuck.append(new_arrow)
